import { Container } from '@mui/material'
import React from 'react'
import styles from './styles.module.css'

const Layout = ({ children }) => {
  return (
    <Container maxWidth='lg' className={styles.container}>
      {children}
    </Container>
  )
}

export default Layout
