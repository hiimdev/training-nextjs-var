import '../styles/globals.css'
import '../styles/variables.css'
import { Provider } from 'react-redux'
import store from '../store'
import Layout from '../layouts'
import baseTheme from '../baseTheme'
import { ThemeProvider } from '@emotion/react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'

function MyApp({ Component, pageProps }) {
  const queryClient = new QueryClient()

  return (
    <ThemeProvider theme={baseTheme}>
      <Provider store={store}>
        <QueryClientProvider client={queryClient}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </QueryClientProvider>
      </Provider>
    </ThemeProvider>
  )
}

export default MyApp
