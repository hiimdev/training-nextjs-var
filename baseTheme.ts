import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 640,
      md: 900,
      lg: 1440,
      xl: 1536,
    },
  },
  palette: {
    primary: {
      main: '#3d00b7',
    },
    secondary: {
      main: '#2639ED',
    },
    error: {
      main: '#BA2440',
    },
    success: {
      main: '#4caf50',
    },
    warning: {
      main: '#FCBE04',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'capitalize',
        },
        outlinedPrimary: {
          border: '2px solid var(--primary-color)',
        },
        sizeMedium: {
          fontWeight: 700,
          fontSize: 14,
          height: 50,
          borderRadius: 30,
          padding: '0 20px',
        },
        sizeLarge: {
          fontWeight: 400,
          fontSize: 20,
          height: 65,
          borderRadius: 60,
          padding: '20px 40px',
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          '& .MuiInput-root:before': {
            border: 'none',
          },
          '& .MuiInput-root:after': {
            border: 'none',
          },
          '& .MuiInput-root:hover:not(.Mui-disabled, .Mui-error):before': {
            border: 'none',
          },
          '& .MuiOutlinedInput-root': {
            '& fieldset': {
              border: 'none',
            },
            '&:hover fieldset': {
              border: 'none',
            },
          },
          '& .MuiInputBase-root.MuiFilledInput-root': {
            backgroundColor: 'unset',
            padding: 'unset',
          },
        },
      },
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          backgroundColor: 'unset',
          padding: 'unset',
          '&:hover:not(.Mui-disabled, .Mui-error):before': {
            border: 'none',
            backgroundColor: 'unset',
          },
          '&:before': {
            border: 'none',
          },
          '&:after': {
            border: 'none',
          },
        },
        input: {
          padding: '2px 6px',
        },
      },
    },
    MuiCardHeader: {
      styleOverrides: {
        root: {
          padding: 0,
        },
        title: {
          fontWeight: 700,
          fontSize: 20,
        },
      },
    },
    MuiCardMedia: {
      styleOverrides: {
        root: {
          minWidth: 103,
        },
      },
    },
  },
  typography: {
    fontFamily: '"DM Sans", sans-serif',
  },
})

export default theme
