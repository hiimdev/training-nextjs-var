import { Button as ButtonMUI } from '@mui/material'
import React from 'react'
import styles from './styles.module.css'
import clsx from 'clsx'
import { ButtonProps } from './types'

export const Button = ({
  children,
  sx,
  className,
  variant,
  color,
  size,
  style,
  onClick,
  ...props
}: ButtonProps) => {
  return (
    <ButtonMUI
      className={clsx(styles.btn, className)}
      variant={variant}
      color={color}
      size={size}
      style={style}
      sx={sx}
      {...props}
    >
      {children}
    </ButtonMUI>
  )
}
