import { SxProps } from '@mui/material'
import { CSSProperties, ReactNode } from 'react'

export interface ButtonProps {
  children: ReactNode
  className?: string
  sx?: SxProps
  variant: 'contained' | 'outlined' | 'text'
  color?:
    | 'inherit'
    | 'primary'
    | 'secondary'
    | 'success'
    | 'error'
    | 'info'
    | 'warning'
  onClick?(event?: MouseEvent): void
  size?: 'small' | 'medium' | 'large'
  style?: CSSProperties
}
