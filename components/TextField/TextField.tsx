import React from 'react'
import { Paper, TextField as TextFieldMUI } from '@mui/material'
import { TextFieldProps } from './types'
import styles from './styles.module.css'

export const TextField = ({
  id,
  variant,
  type,
  className,
  value,
  onChange,
  style,
  children,
  ...props
}: TextFieldProps) => {
  return (
    <Paper className={styles.wrapTextField}>
      <TextFieldMUI
        id={id}
        variant={variant}
        type={type}
        className={className}
        value={value}
        onChange={onChange}
        style={style}
        {...props}
      />
      {children}
    </Paper>
  )
}
