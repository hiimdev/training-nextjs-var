import React, { CSSProperties, ChangeEvent, ReactNode } from 'react'
import { SxProps, TextFieldProps as TextFieldPropsMUI } from '@mui/material'

export interface TextFieldProps extends Omit<TextFieldPropsMUI, 'customProp'> {
  label?: string
  id?: string
  defaultValue?: string
  variant?: 'filled' | 'outlined' | 'standard'
  size?: 'medium' | 'small'
  type?: string
  sx?: SxProps
  className?: string
  placeholder?: string
  disabled?: boolean
  value?: any
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
  style?: CSSProperties
  helperText?: string
  InputProps?: any
  children?: ReactNode
}
