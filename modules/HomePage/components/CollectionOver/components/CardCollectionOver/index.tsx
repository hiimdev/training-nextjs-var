import { Card, CardMedia, Avatar, Box, Typography } from '@mui/material'
import React from 'react'
import styles from './styles.module.css'
import Image from 'next/image'
import { Button } from '../../../../../../components/Button'
import { SxProps } from '@mui/material'

export interface CardCollectionOverProps {
  src: string
  title: string
  srcAvt?: string
  price: string | number
  desc: string
  sx?: SxProps
  active?: boolean
}

const CardCollectionOver = ({
  src,
  title,
  srcAvt,
  price,
  desc,
  sx,
  active,
  ...props
}: CardCollectionOverProps) => {
  return (
    <Card sx={sx}>
      <CardMedia
        sx={{ borderRadius: '12px', marginRight: 2 }}
        component='img'
        width='147'
        height='147'
        image={src}
        alt=''
      />
      <Box className={styles.wrapRight}>
        <Typography className={styles.title}>{title}</Typography>
        <Box className={styles.wrapUser}>
          <Avatar className={!srcAvt ? styles.avt : ''} aria-label='recipe'>
            {srcAvt ? (
              <Image alt='' src={srcAvt} width={34} height={34} />
            ) : (
              'A'
            )}
          </Avatar>
          <Typography className={styles.price}>
            <Image alt='' src={'/images/ethereum3.png'} width={8} height={14} />{' '}
            {price} ETH
          </Typography>
          <Typography className={styles.desc}>{desc}</Typography>
        </Box>

        <Button
          variant="outlined"
          sx={{
            '&:hover': {
              backgroundColor: 'var(--primary-color)',
              color: 'var(--white-color)',
            },
          }}
        >
          Place a bid
        </Button>
      </Box>
    </Card>
  )
}

export default CardCollectionOver
