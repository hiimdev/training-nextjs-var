import React from 'react'
import { Avatar, Box, Card, Typography } from '@mui/material'
import styles from './styles.module.css'
import Image from 'next/image'

export interface CardTopCollectionProps {
  title: string
  srcAvt?: string
  price: string
  percent: string
  hot?: boolean
  tick?: boolean
}

const CardTopCollection = ({
  title,
  srcAvt,
  price,
  percent,
  hot,
  tick,
  ...props
}: CardTopCollectionProps) => {
  return (
    <Card sx={{ maxWidth: 400, boxShadow: 'none' }}>
      <Box className={styles.container}>
        <Box sx={{ position: 'relative' }}>
          {tick && (
            <Image
              className={styles.tick}
              alt=''
              src={'/images/tick.png'}
              width={26}
              height={26}
            />
          )}
          <Avatar className={styles.avt}>
            {srcAvt ? (
              <Image alt='' src={srcAvt} width={60} height={60} />
            ) : (
              'A'
            )}
          </Avatar>
        </Box>
        <Box ml={3} mr={4}>
          <Typography className={styles.title}>{title}</Typography>
          <Typography
            className={styles.price}
            sx={{
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <Image
              style={{ marginRight: 10 }}
              alt=''
              src={'/images/Vector.png'}
              width={15}
              height={24}
            />{' '}
            {price}
          </Typography>
        </Box>
        <Box>
          <Typography
            sx={hot ? { color: '#FF002E' } : { color: '#14C8B0' }}
            className={styles.percent}
          >
            {percent}
          </Typography>
        </Box>
      </Box>
    </Card>
  )
}

export default CardTopCollection
