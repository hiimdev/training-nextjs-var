import React from 'react'
import styles from './styles.module.css'
import {
  Avatar,
  Box,
  Card,
  CardHeader,
  CardMedia,
  Typography,
  Divider,
} from '@mui/material'
import Image from 'next/image'
import CardCollectionOver from './components/CardCollectionOver'
import CardTopCollection from './components/CardTopCollection'

const CollectionOver = () => {
  const items = [1, 2, 3, 4, 5]
  const lastItem = items[items.length - 1]

  return (
    <Box
      className={styles.container}
      sx={{
        mt: { xs: 140, md: 120, lg: 70 },
      }}
    >
      <Card
        sx={{
          maxWidth: 400,
          boxShadow: 'none',
          mb: { xs: 10, lg: 'none' },
        }}
      >
        <CardMedia
          component='img'
          width='400px'
          height='424px'
          sx={{ borderRadius: '24px' }}
          image='/images/unsplash_F56Y7dgrAkc.png'
          alt=''
        />
        <Box className={styles.wrapRight}>
          <CardHeader
            avatar={
              <Avatar sx={{ bgcolor: '#3f78c7' }} aria-label='recipe'>
                H
              </Avatar>
            }
            title='The Futr Abstr'
            subheader='10 in the stock'
          />
          <Box>
            <Typography className={styles.titleRight}>Highest Bid</Typography>
            <Typography className={styles.price}>
              <Image
                alt=''
                src={'/images/Vector.png'}
                width={12}
                height={21}
                style={{ marginRight: 10 }}
              />
              0.25 ETH
            </Typography>
          </Box>
        </Box>
      </Card>
      <Box
        sx={{
          ml: { xs: 0, md: '1rem', lg: '4rem' },
          mb: { xs: 5, md: 0 },
        }}
      >
        {[1, 2, 3].map((i) => (
          <CardCollectionOver
            key={i}
            sx={{ maxWidth: 400, display: 'flex', boxShadow: 'none', mb: 4 }}
            src={'/images/unsplash_wHJ5L9KGTl4.png'}
            srcAvt={'/images/Ellipse95.png'}
            title={'The Futr Abstr'}
            price={'0.25'}
            desc={'1 of 8'}
            active={i === 1 ? true : false}
          />
        ))}
      </Box>
      <Divider
        orientation={'vertical'}
        sx={{ display: { xs: 'none', lg: 'block' }, height: '32rem' }}
      />
      <Box
        className={styles.wrapTopCollection}
        sx={{
          ml: { xs: 0, lg: '3rem' },
        }}
      >
        <Typography className={styles.title}>Top Collections over</Typography>
        <Typography className={styles.desc}>Last 7 days</Typography>
        {items.map((i, index) =>
          index % 2 === 0 ? (
            <div key={i}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography className={styles.index} mr={3}>
                  {index + 1}
                </Typography>
                <CardTopCollection
                  tick={true}
                  hot={true}
                  srcAvt={'/images/unsplash_WjIB-6UxA5Q.png'}
                  title={'CryptoFunks'}
                  price={'19,769.39'}
                  percent={'+26.52%'}
                />
              </Box>
              {i !== lastItem && (
                <Divider sx={{ m: '17px 0', width: '21rem' }} />
              )}
            </div>
          ) : (
            <div key={i}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Typography className={styles.index} mr={3}>
                  {index + 1}
                </Typography>
                <CardTopCollection
                  srcAvt={'/images/unsplash_5MTf9XyVVgM.png'}
                  title={'CryptoFunks'}
                  price={'19,769.39'}
                  percent={'+26.52%'}
                />
              </Box>
              {i !== lastItem && (
                <Divider sx={{ m: '17px 0', width: '21rem' }} />
              )}
            </div>
          )
        )}
      </Box>
    </Box>
  )
}

export default CollectionOver
