import React from 'react'
import styles from './styles.module.css'
import {
  Card,
  CardMedia,
  Box,
  CardHeader,
  Avatar,
  Typography,
} from '@mui/material'
import styled from '@emotion/styled';
import { Button } from '../../../../../components/Button'

export interface CardCollectionNFTProps {
  srcBigImg: string
  srcImg1: string
  srcImg2: string
  srcImg3: string
  avatar?: string
  totalItem: number
  author: string
}

const CardCollectionNFT = ({
  srcBigImg,
  srcImg1,
  srcImg2,
  srcImg3,
  avatar,
  totalItem,
  author,
}: CardCollectionNFTProps) => {
  // const StyledCardHeader = styled(CardHeader)(({ theme }) => ({
  //   '& .MuiTypography-root': {
  //     fontSize: '14px',
  //     fontWeight: 500,
  //   },
  //   '& .MuiCardHeader-avatar': {
  //     marginRight: '10px !important',
  //   },
  // }))

  return (
    <Card
      sx={{
        boxShadow: 'none',
        background: 'transparent',
        mb: { xs: 10, lg: 'none' },
      }}
    >
      <Box sx={{ display: 'flex' }}>
        <CardMedia
          component='img'
          sx={{ borderRadius: '24px', mr: 1, height: '272px', objectFit: 'cover' }}
          image={srcBigImg}
          alt=''
        />
        <Box>
          <CardMedia
            component='img'
            sx={{ width: '103px', height: '85px', borderRadius: '24px', mb: 1 }}
            image={srcImg1}
            alt=''
          />
          <CardMedia
            component='img'
            sx={{ width: '103px', height: '85px', borderRadius: '24px', mb: 1 }}
            image={srcImg2}
            alt=''
          />
          <CardMedia
            component='img'
            sx={{ width: '103px', height: '85px', borderRadius: '24px', mb: 1 }}
            image={srcImg3}
            alt=''
          />
        </Box>
      </Box>
      <Box className={styles.wrapRight}>
        <Box>
          <Typography className={styles.collectionName}>
            Amazing Collection
          </Typography>
          <CardHeader
            avatar={
              <Avatar
                src={avatar}
                sx={{ bgcolor: '#3f78c7', mr: '0 !important' }}
                aria-label='recipe'
              >
                H
              </Avatar>
            }
            title={`by ${author}`}
          />
        </Box>
        <Box>
          <Button
            variant='outlined'
            sx={{
              '&:hover': {
                backgroundColor: 'var(--primary-color)',
                color: 'var(--white-color)',
              },
            }}
          >
            Total {totalItem} Items
          </Button>
        </Box>
      </Box>
    </Card>
  )
}

export default CardCollectionNFT
