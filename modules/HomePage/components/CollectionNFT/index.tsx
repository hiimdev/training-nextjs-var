import React from 'react'
import styles from './styles.module.css'
import { Box, Container, Typography } from '@mui/material'
import CardCollectionNFT from './CardCollectionNFT'

const CollectionNFT = () => {
  return (
    <Box
      className={styles.containerFluid}
      sx={{
        mt: { xs: 10 },
      }}
    >
      <Container
        maxWidth='lg'
        className={styles.container}
        sx={{
          flexDirection: { xs: 'column' },
          alignItems: { xs: 'center', md: 'flex-start' },
          textAlign: { xs: 'center', md: 'unset' },
        }}
      >
        <Typography className={styles.title} sx={{ mb: { xs: 10 } }}>
          Collection Featured NFTs
        </Typography>
        <Box
          sx={{
            width: '100%',
            display: 'grid',
            gridTemplateColumns: 'repeat(3, 1fr)',
            gap: 4,
            '@media (max-width: 1300px)': {
              gridTemplateColumns: 'repeat(2, 1fr)',
            },
          
            '@media (max-width: 850px)': {
              gridTemplateColumns: '1fr',
            },
          }}
        >
          <CardCollectionNFT
            avatar='/images/unsplash_WjIB-6UxA5Q.png'
            srcBigImg='/images/unsplash_F56Y7dgrAkc.png'
            srcImg1='/images/unsplash_LpbyDENbQQg.png'
            srcImg2='/images/unsplash_pVoEPpLw818.png'
            srcImg3='/images/unsplash_tZCrFpSNiIQ.png'
            totalItem={54}
            author='HuiDev'
          />
          <CardCollectionNFT
            avatar='/images/unsplash_5MTf9XyVVgM.png'
            srcBigImg='/images/unsplash_E8Ufcyxz514.png'
            srcImg1='/images/unsplash_9anj7QWy-2g.png'
            srcImg2='/images/unsplash_tZCrFpSNiIQ.png'
            srcImg3='/images/unsplash_LpbyDENbQQg.png'
            totalItem={163}
            author='Huine'
          />
          <CardCollectionNFT
            avatar='/images/unsplash_WjIB-6UxA5Q.png'
            srcBigImg='/images/Group882.png'
            srcImg1='/images/unsplash_wHJ5L9KGTl4.png'
            srcImg2='/images/unsplash_LpbyDENbQQg.png'
            srcImg3='/images/unsplash_pVoEPpLw818.png'
            totalItem={21}
            author='Arkhan17'
          />
        </Box>
      </Container>
    </Box>
  )
}

export default CollectionNFT
