import { Box, Typography } from '@mui/material'
import React from 'react'
import Image from 'next/image'
import styles from './styles.module.css'

const Right = () => {
  return (
    <Box
      className={styles.containerRight}
      sx={{
        mt: { xs: 7, md: 10, lg: 0 },
        ml: { xs: 0, md: '50%', lg: 20 },
      }}
    >
      <Box className={styles.wrapUser}>
        <Typography className={styles.titleRight}>
          Abstr Gradient NFT
        </Typography>
        <Typography className={styles.name}>
          <Image
            className={styles.avt}
            alt=''
            src={'/images/Ellipse95.png'}
            width={30}
            height={30}
          />
          Arkhan17
        </Typography>
      </Box>
      <Image
        className={styles.stImg}
        alt=''
        src={'/images/Group83.png'}
        width={130}
        height={130}
      />
      <Image
        className={styles.ndImg}
        alt=''
        src={'/images/unsplash_E8Ufcyxz514.png'}
        width={400}
        height={440}
      />
      <Image
        className={styles.rdImg}
        alt=''
        src={'/images/unsplash_pVoEPpLw818.png'}
        width={356}
        height={391}
      />
      <Image
        className={styles.thImg}
        alt=''
        src={'/images/unsplash_tZCrFpSNiIQ.png'}
        width={310}
        height={341}
      />
      <Box className={styles.wrapCurBid}>
        <Typography className={styles.titleRightBot}>
          Current Bid{' '}
          <span>
            <Image
              style={{ marginRight: 9 }}
              alt=''
              src={'/images/ethereum2.png'}
              width={12}
              height={21}
            />{' '}
            0.25 ETH
          </span>
        </Typography>
      </Box>
      <Box className={styles.wrapEndDay}>
        <Typography className={styles.textDay}>
          Ends in
          <span>12h 43m 42s</span>
        </Typography>
      </Box>
      <Image
        className={styles.bgImg}
        alt=''
        src={'/images/Rectangle183.png'}
        width={348}
        height={73}
      />
    </Box>
  )
}

export default Right
