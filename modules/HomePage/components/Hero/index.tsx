import React from 'react'
import { Box } from '@mui/material'
import Left from './Left'
import Right from './Right'
import styles from './styles.module.css'

const Hero = () => {
  return (
    <Box
      className={styles.container}
      sx={{ flexDirection: { xs: 'column', lg: 'row' } }}
    >
      <Left />
      <Right />
    </Box>
  )
}

export default Hero
