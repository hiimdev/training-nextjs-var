import React from 'react'
import { Box, Typography } from '@mui/material'
import styles from './styles.module.css'
import Image from 'next/image'
import { Button } from '../../../../components/Button'

const Left = () => {
  return (
    <Box>
      <Typography
        className={styles.title}
        sx={{ width: { xs: 'unset', md: '612px' } }}
      >
        Discover, and collect Digital Art NFTs
      </Typography>
      <Typography className={styles.desc}>
        Digital marketplace for crypto collectibles and non-fungible tokens
        (NFTs). Buy, Sell, and discover exclusive digital assets.
      </Typography>
      <Box position={'relative'} mt={5}>
        <Image
          className={styles.dotImg}
          src={'/images/Dot.png'}
          width={196}
          height={154}
          alt=''
        />
        <Button
          sx={{
            width: '209px',
            '&:hover': {
              color: 'var(--primary-color)',
              backgroundColor: 'var(--white-color)',
              border: '2px solid var(--primary-color)',
            },
            ml: 1,
          }}
          variant='contained'
          size='large'>
          Explore Now
        </Button>
        <Box className={styles.wrapNum}>
          <Typography className={styles.num}>
            98k+ <span>Artwork</span>
          </Typography>
          <Typography className={styles.num}>
            12k+ <span>Auction</span>
          </Typography>
          <Typography className={styles.num}>
            15k+ <span>Artist</span>
          </Typography>
        </Box>
      </Box>
    </Box>
  )
}

export default Left
