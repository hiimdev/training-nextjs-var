import { Box, Container, Typography } from '@mui/material'
import React from 'react'
import styles from './styles.module.css'
import CreditScoreIcon from '@mui/icons-material/CreditScore'
import PollIcon from '@mui/icons-material/Poll'

const AmazeNFT = () => {
  return (
    <Box
      className={styles.containerFluid}
      sx={{
        mt: { xs: 50, lg: 0 },
      }}
    >
      <Container
        maxWidth='lg'
        className={styles.container}
        sx={{
          flexDirection: { xs: 'column', md: 'row' },
          alignItems: { xs: 'center', md: 'flex-start' },
          textAlign: { xs: 'center', md: 'unset' },
        }}
      >
        <Typography
          className={styles.title}
          sx={{ marginBottom: { xs: 5, md: 'unset' } }}
        >
          The amazing NFT art of the world here
        </Typography>
        <Box className={styles.wrapFast}>
          <CreditScoreIcon sx={{ fontSize: '2.5rem' }} />
          <Box className={styles.wraptext}>
            <Typography className={styles.titleFast}>
              Fast Transaction
            </Typography>
            <Typography className={styles.desc}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
              etiam viverra tellus imperdiet.
            </Typography>
          </Box>
        </Box>
        <Box className={styles.wrapGrowth}>
          <PollIcon sx={{ fontSize: '2.5rem' }} />
          <Box className={styles.wraptext}>
            <Typography className={styles.titleGrowth}>
              Growth Transaction
            </Typography>
            <Typography className={styles.desc}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
              etiam viverra tellus
            </Typography>
          </Box>
        </Box>
      </Container>
    </Box>
  )
}

export default AmazeNFT
