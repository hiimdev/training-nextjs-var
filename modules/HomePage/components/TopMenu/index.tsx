import {
  Grid,
  IconButton,
  InputBase,
  Paper,
  Typography,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemText,
  Container,
} from '@mui/material'
import React from 'react'
import styles from './styles.module.css'
import SearchIcon from '@mui/icons-material/Search'
import MenuIcon from '@mui/icons-material/Menu'
import StoreIcon from '@mui/icons-material/Store'
import SourceIcon from '@mui/icons-material/Source'
import InfoIcon from '@mui/icons-material/Info'
import { Button } from '../../../../components/Button'
import Image from 'next/image'
import { TextField } from '../../../../components/TextField'
import CloseIcon from '@mui/icons-material/Close';

const DrawerLeft = ({ open, onClose }) => {
  return (
    <Drawer
      sx={{
        '& .MuiDrawer-paper': {
          width: '50%',
          padding: '15px'
        },
      }}
      className={styles.drawer}
      open={open}
      onClose={onClose}
    >
      <CloseIcon
        sx={{
          position: 'absolute',
          top: 10,
          right: 10,
          fontSize: 35,
        }}
        onClick={onClose}
      />
      <List sx={{ mt: 5 }}>
        <ListItem className={styles.itemMobile}>
          <StoreIcon />
          <ListItemText className={styles.itemText} primary='Marketplace' />
        </ListItem>
        <Divider />
        <ListItem className={styles.itemMobile}>
          <SourceIcon />
          <ListItemText className={styles.itemText} primary='Resource' />
        </ListItem>
        <Divider />
        <ListItem className={styles.itemMobile}>
          <InfoIcon />
          <ListItemText className={styles.itemText} primary='About' />
        </ListItem>
        <Divider />
      </List>
      <TextField sx={{ ml: 1, flex: 1 }} placeholder='Search'>
        <IconButton type='button' sx={{ p: '10px' }} aria-label='search'>
          <SearchIcon />
        </IconButton>
      </TextField>
      <Button
        variant='contained'
        size='medium'
        sx={{
          '&:hover': {
            color: 'var(--primary-color)',
            backgroundColor: 'var(--white-color)',
            boxShadow: 'none',
            border: '2px solid var(--primary-color)'
          },
          mt: 2,
          boxShadow: 'none'
        }}
      >
        Upload
      </Button>
      <Button
        variant='outlined'
        size='medium'
        sx={{
          '&:hover': {
            backgroundColor: 'var(--primary-color)',
            color: 'var(--white-color)',
          },
          mt: 2,
        }}
      >
        Connect Wallet
      </Button>
    </Drawer>
  )
}

const TopMenu = () => {
  const [drawerOpen, setDrawerOpen] = React.useState(false)

  const handleDrawerOpen = () => {
    setDrawerOpen(true)
  }

  const handleDrawerClose = () => {
    setDrawerOpen(false)
  }

  return (
    <>
      <Container maxWidth='lg' className={styles.container}>
        <MenuIcon
          sx={{
            display: { md: 'block', lg: 'none' },
            fontSize: 40,
            cursor: 'pointer',
          }}
          onClick={handleDrawerOpen}
        ></MenuIcon>
        <Grid
          sx={{
            mr: 4,
            '&:hover': {
              transform: 'scale(1.1)',
              transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
            },
            '&:not(:hover)': {
              transform: 'scale(1) translate(0, 0)',
              transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
            },
          }}
          item
        >
          <Image
            style={{ cursor: 'pointer' }}
            src={'/images/NFters.png'}
            alt='logo'
            width={107}
            height={20}
          />
        </Grid>
        <Divider
          sx={{ height: '40px', display: { xs: 'none', lg: 'block' }, mr: 2 }}
          orientation='vertical'
        ></Divider>
        <Grid
          item
          sx={{
            display: { xs: 'none', md: 'none', lg: 'flex' },
            alignItems: 'center',
            textAlign: 'center',
            justifyContent: 'space-around',
          }}
        >
          <Typography
            sx={{
              '&:hover': {
                transform: 'scale(1.1)',
                transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
              },
              '&:not(:hover)': {
                transform: 'scale(1) translate(0, 0)',
                transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
              },
            }}
            className={styles.menuItem}
          >
            Marketplace
          </Typography>
          <Typography
             sx={{
              '&:hover': {
                transform: 'scale(1.1)',
                transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
              },
              '&:not(:hover)': {
                transform: 'scale(1) translate(0, 0)',
                transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
              },
            }}
            className={styles.menuItem}
          >
            Resource
          </Typography>
          <Typography
            sx={{
              '&:hover': {
                transform: 'scale(1.1)',
                transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
              },
              '&:not(:hover)': {
                transform: 'scale(1) translate(0, 0)',
                transition: 'transform 0.5s cubic-bezier(0.25, 0.1, 0.25, 1)'
              },
            }}
            className={styles.menuItem}
          >
            About
          </Typography>
        </Grid>
        <Grid
          className={styles.wrapRight}
          sx={{ display: { xs: 'none', md: 'none', lg: 'flex' } }}
          item
        >
          <TextField sx={{ ml: 1, flex: 1 }} placeholder='Search'>
            <IconButton type='button' sx={{ p: '10px' }} aria-label='search'>
              <SearchIcon />
            </IconButton>
          </TextField>
          <Button
            sx={{
              '&:hover': {
                color: 'var(--primary-color)',
                backgroundColor: 'var(--white-color)',
                boxShadow: 'none',
                border: '2px solid var(--primary-color)',
              },
              ml: 1,
            }}
            variant='contained'
          >
            Upload
          </Button>
          <Button
            sx={{
              '&:hover': {
                backgroundColor: 'var(--primary-color)',
                color: 'var(--white-color)',
              },
              ml: 1,
            }}
            variant='outlined'
          >
            Connect Wallet
          </Button>
        </Grid>
      </Container>
      <Divider className={styles.lineTopMenu} sx={{ mt: 1 }} />
      <DrawerLeft open={drawerOpen} onClose={handleDrawerClose} />
    </>
  )
}

export default TopMenu
