import React from 'react'
import TopMenu from './components/TopMenu'
import Hero from './components/Hero'
import AmazeNFT from './components/AmazeNFT'
import CollectionOver from './components/CollectionOver'
import CollectionNFT from './components/CollectionNFT'

const HomePage = () => {
  return (
    <>
      <TopMenu />
      <Hero />
      <AmazeNFT />
      <CollectionOver />
      <CollectionNFT />
    </>
  )
}

export default HomePage
